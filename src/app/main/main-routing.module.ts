import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { HeaderComponent } from './header/header.component';
import { BannerComponent } from './banner/banner.component';
import { CardComponent } from './card/card.component';
import { SearchBarComponent } from './search-bar/search-bar.component';
import { FooterComponent} from './footer/footer.component';
import {AboutComponent} from './about/about.component';
const routes: Routes = [
  { path: 'header', component: HeaderComponent },
  { path: 'search-bar', component: SearchBarComponent },
   {path: 'about', component:AboutComponent},
  { path: 'banner', component: BannerComponent },
  { path: 'card', component: CardComponent },
  { path: 'footer', component: FooterComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MainRoutingModule { }
