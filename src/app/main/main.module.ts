import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MainRoutingModule } from './main-routing.module';
import { AboutComponent } from './about/about.component';
// import { HeaderComponent } from './header/header.component';
// import { SearchBarComponent } from './search-bar/search-bar.component';
// import { BannerComponent } from './banner/banner.component';
// import { CardComponent } from './card/card.component';
// import { FooterComponent } from './footer/footer.component';


@NgModule({
  declarations: [
    // HeaderComponent,
    // SearchBarComponent,
    // BannerComponent,
    // CardComponent,
    // FooterComponent
  
    // AboutComponent
  ],
  imports: [
    CommonModule,
    MainRoutingModule
  ]
})
export class MainModule { }
